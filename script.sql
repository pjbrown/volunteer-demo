USE volunteers;

DROP TABLE IF EXISTS volunteer_availability;
DROP TABLE IF EXISTS volunteer;
DROP TABLE IF EXISTS volunteer_account;

CREATE TABLE volunteer_account
(
    username VARCHAR(32) UNIQUE NOT NULL,
    password_hash VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,

    PRIMARY KEY (username)
);

CREATE TABLE volunteer
(
    volunteer_id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(32) NOT NULL,
    date_registration DATETIME,

    firstname VARCHAR(32) NOT NULL,
    lastname VARCHAR(32) NOT NULL,
    mobile CHAR(15),
    presentation_description VARCHAR(1024),
    organisations VARCHAR(1024),
    mailinglist BOOLEAN,

    PRIMARY KEY (volunteer_id),
    FOREIGN KEY (username) REFERENCES volunteer_account(username)
);

CREATE TABLE volunteer_availability
(
    date_availability DATE NOT NULL,
    volunteer_id INT NOT NULL,
    availability_location CHAR(16),

    PRIMARY KEY (volunteer_id, date_availability),
    FOREIGN KEY (volunteer_id) REFERENCES volunteer(volunteer_id)
);