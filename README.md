# Volunteer Signup Demo Source

Pure PHP web site and registration system customised for specific use case of allowing returning users to register quickly by automatically filling out forms with details from a previous registration. Developed as proof of concept/demo for specific educational organisation.

![Registration Page](https://i.imgur.com/6Qc4LJ1.png)

Volunteer by filling out form. Details from a previous registration may automatically fill out parts of the form, speeding up the process of repeated volunteering each semester.

![Volunteer Page](https://i.imgur.com/bAWOlG4.png)

## Requirements

* PHP 5.6
* MySQL

## Notes

* Specific references to the organisation for which the original purpose of the demo site was developed have been removed from the code for privacy.