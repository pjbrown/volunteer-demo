<?php
class AuthController
{
    public function __construct()
    {
        session_start();
        
        if (isset($_SESSION["last_activity"]) && time() > $_SESSION["last_activity"] + 1800)
        {
            $this->logout();
        }
        $_SESSION["last_activity"] = time();
    }
    
    public function isLoggedIn()
    {
        if (isset($_SESSION["auth"]))
        {
            return $_SESSION["auth"];
        }
        else
        {
            return false;
        }
    }
    
    public function logout()
    {
        unset($_SESSION["auth"]);
        unset($_SESSION["username"]);
        unset($_SESSION["last_activity"]);
        session_destroy();
    }
    
    public function getUsername()
    {
        return $_SESSION["username"];
    }
}
$c_auth = new AuthController();