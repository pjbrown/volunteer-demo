<?php
class LoginController
{
    private $errors;
    private $view;
    
    private $username;
    private $password;
    
    public function __construct()
    {
        global $c_auth, $m;
        
        $this->view = "default";
        
        if ($c_auth->isLoggedIn())
        {
            $this->view = "loggedin";
        }
        else if (filter_input(INPUT_SERVER, "REQUEST_METHOD") == "POST")
        {
            $args = [
                "username" => FILTER_SANITIZE_STRING,
                "password" => FILTER_SANITIZE_STRING
            ];
            $values = filter_input_array(INPUT_POST, $args);
            
            $this->setUsername($values["username"]);
            $this->setPassword($values["password"]);
            
            if (!$this->errors)
            {
                $m->setUsername($this->username);
                $m->setPassword($this->password);
                if ($m->authenticate())
                {
                    $this->view = "success";
                    
                    $_SESSION["auth"] = true;
                    $_SESSION["username"] = $this->username;
                }
                else
                {
                    $this->errors["login"] = "Password did not match.";
                }
            } 
        }
    }
    
    public function showView()
    {
        global $c, $m;
        switch ($this->view)
        {
            case "loggedin":
                include "../src/view/message.php";
                break;
            case "fail":
                include "../src/view/message.php";
                break;
            case "success":
                include "../src/view/message.php";
                break;
            default:
                include "../src/view/login.php";
                break;
        }
    }
    
    public function getMessage()
    {
        switch ($this->view)
        {
            case "loggedin":
                return "You are already logged in.";
            default:
                return "Login successful.";
        }
    }
    
    public function getError($key)
    {
        if (isset($this->errors[$key]))
        {
            return $this->errors[$key];
        }
        return "";
    }
    
    public function isLoggedIn()
    {
        return false;
    }
    
    public function setUsername($input)
    {
        $this->username = $input;
        if (empty($this->username))
        {
            $this->errors["username"] = "A username is required.";
        }
    }
    
    public function getUsername()
    {
        return $this->username;
    }
    
    public function setPassword($input)
    {
        $this->password = $input;
        if (empty($this->password))
        {
            $this->errors["password"] = "A password is required.";
        }
    }
}
$c = new LoginController();