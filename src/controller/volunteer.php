<?php
/**
 * 
 */
class VolunteerController
{
    private $view;
    
    private $days = [
        "2015-08-22T12:30:00",
        "2015-08-29T12:30:00",
        "2015-09-05T12:30:00",
        "2015-09-12T12:30:00",
        "2015-09-19T12:30:00",
        "2015-10-03T12:30:00",
        "2015-10-10T12:30:00",
        "2015-10-17T12:30:00",
    ];
    
    private $firstName;
    private $lastName;
    private $mobile;
    private $description;
    private $organisations;
    private $availability = [];
    private $mailing;
    
    private $returning;
    
    private $errors;
    
    public function __construct()
    {
        global $m, $c_auth, $c_register, $m_register;
        
        $this->view = "default";
        $this->returning = false;
        
        if (filter_input(INPUT_SERVER, "REQUEST_METHOD") == "POST")
        {
            // collect basic values
            $args = [
                "firstname" => FILTER_SANITIZE_STRING,
                "lastname" => FILTER_SANITIZE_STRING,
                "email" => FILTER_SANITIZE_EMAIL,
                "mobile" => FILTER_SANITIZE_STRING,
                "description" => FILTER_SANITIZE_STRING,
                "organisations" => FILTER_SANITIZE_STRING,
                "mailing" => ["filter" => FILTER_VALIDATE_BOOLEAN, "flags" => FILTER_NULL_ON_FAILURE],
            ];
            $values = filter_input_array(INPUT_POST, $args);
            
            $this->setFirstName($values["firstname"]);
            $this->setLastName($values["lastname"]);
            $this->setMobile($values["mobile"]);
            $this->setDescription($values["description"]);
            $this->setOrganisations($values["organisations"]);
            $this->setAvailability();
            $this->setMailing($values["mailing"]);
            
            if (!$this->errors && !$c_register->hasErrors())
            {
                // if we aren't logged in, do the account registration first
                if (!$c_auth->isLoggedIn())
                {
                    $c_register->register();
                }

                // volunteer registration here
                $m->setUsername($c_auth->getUsername());
                $m->setFirstName($this->firstName);
                $m->setLastName($this->lastName);
                $m->setMobile($this->mobile);
                $m->setDescription($this->description);
                $m->setOrganisations($this->organisations);
                $m->setAvailability($this->availability);
                $m->setMailing($this->mailing);
                
                try
                {
                    $m->insert();
                    $this->view = "success";
                }
                catch (PDOException $ex)
                {
                    echo $ex->getMessage();
                }
            }
        }
        // restore last entry if logged in
        else if ($c_auth->isLoggedIn())
        {
            $m->setUsername($c_auth->getUsername());
            if ($m->getLast())
            {
                $this->setFirstName($m->getFirstName());
                $this->setLastName($m->getLastName());
                $this->setMobile($m->getMobile());
                $this->setDescription($m->getDescription());
                $this->setOrganisations($m->getOrganisations());
                //$this->setAvailability($m->getAvailability());
                $this->setMailing($m->getMailing());

                $this->returning = true;
            }
        }
    }
    
    public function showView()
    {
        global $c_auth, $c_register, $c, $m;
        switch ($this->view)
        {
            case "success":
                include "../src/view/message.php";
                break;
            default:
                include "../src/view/volunteer.php";
                break;
        }
    }
    
    public function getMessage()
    {
        switch ($this->view)
        {
            case "success":
                return "Volunteer success";
            default:
                return "Hi.";
        }
    }
    
    public function getError($key)
    {
        if (isset($this->errors[$key]))
        {
            return $this->errors[$key];
        }
        return "";
    }
    
    public function setFirstName($input)
    {
        $this->firstName = $input;
        if (empty($input))
        {
            $this->errors["firstname"] = "This field is required.";
        }
    }
    
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    public function setLastName($input)
    {
        $this->lastName = $input;
        if (empty($input))
        {
            $this->errors["lastname"] = "This field is required.";
        }
    }
    
    public function getLastName()
    {
        return $this->lastName;
    }
    
    public function setMobile($input)
    {
        $this->mobile = $input;
        if (empty($input))
        {
            $this->errors["mobile"] = "This field is required.";
        }
        else if (!filter_var($input, FILTER_VALIDATE_INT))
        {
            $this->errors["mobile"] = "Not a valid mobile number, please only use numbers";
        }
    }
    
    public function getMobile()
    {
        return $this->mobile;
    }
    
    public function setDescription($input)
    {
        $this->description = $input;
        if (empty($input))
        {
            $this->errors["description"] = "This field is required.";
        }
    }
    
    public function getDescription()
    {
        return $this->description;
    }
    
    public function setOrganisations($input)
    {
        $this->organisations = $input;
        if (empty($input))
        {
            $this->errors["organisations"] = "This field is required.";
        }
    }
    
    public function getOrganisations()
    {
        return $this->organisations;
    }
    
    public function getDays()
    {
        return $this->days;
    }
    
    public function setAvailability()
    {
        // collect availability values
        foreach ($this->days as $date)
        {
            $this->availability[$date] = filter_input(INPUT_POST, $date);
            if (empty($this->availability[$date]))
            {
                $this->errors["availability"] = "One or more days were not specified";
            }
        }
    }
    
    public function getAvailability($date, $location)
    {
        if (isset($this->availability[$date]))
        {
            return strcmp($this->availability[$date], $location) == 0;
        }
        else
        {
            return false;
        }
    }
    
    public function getFormattedDate($string)
    {
        $date = new DateTimeImmutable($string);
        $end = $date->add(new DateInterval("PT2H"));
        return $date->format("l j F: g:ia") . " - " . $end->format("g:ia");
    }
    
    public function setMailing($input)
    {
        $this->mailing = $input;
        if (is_null($input))
        {
            $this->errors["mailing"] = "Please select an option.";
        }
    }
    
    public function getMailing()
    {
        return $this->mailing;
    }
    
    public function isMailingChecked($yes)
    {
        if (is_null($this->mailing))
        {
            return "";
        }

        return $yes == $this->mailing ? "checked" : "";
    }
    
    public function isReturning()
    {
        return $this->returning;
    }
}
$c = new VolunteerController();