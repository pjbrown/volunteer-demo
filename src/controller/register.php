<?php
/**
 * 
 */
class RegisterController
{
    private $errors;
    private $view;
    
    private $username;
    private $email;
    private $password;
    private $confirmPassword;
    
    public function __construct()
    {
        global $c, $m_register, $c_auth;
        
        $this->view = "default";
        
        if ($c_auth->isLoggedIn())
        {
            $this->view = "loggedin";
        }
        else if (!isset($c) && filter_input(INPUT_SERVER, "REQUEST_METHOD") == "POST")
        {
            $this->register();
        }
    }
    
    public function register()
    {
        global $m_register, $c_auth;
       
        $args = [
            "username" => FILTER_SANITIZE_STRING,
            "email" => FILTER_SANITIZE_EMAIL,
            "password" => FILTER_SANITIZE_STRING,
            "confirmpassword" => FILTER_SANITIZE_STRING
        ];
        $values = filter_input_array(INPUT_POST, $args);

        $this->setUsername($values["username"]);
        $this->setEmail($values["email"]);
        $this->setPassword($values["password"]);
        $this->setConfirmPassword($values["confirmpassword"]);

        if (!$this->errors)
        {
            $m_register->setUsername($this->username);
            $m_register->setEmail($this->email);
            $m_register->setPassword($this->password);
            try
            {
                if ($m_register->validInsert())
                {
                    $m_register->insert();
                    
                    $this->view = "success";

                    // log us in
                    $_SESSION["auth"] = true;
                    $_SESSION["username"] = $this->username;
                }
                else
                {
                    $this->errors["username"] = "This username is already in use";
                }
            }
            catch (PDOException $ex)
            {
                echo $ex->getMessage();
            }
        }
    }
    
    public function showView()
    {
        global $c_register, $m_register;
        switch ($this->view)
        {
            case "loggedin":
                include "../src/view/messageRegister.php";
                break;
            case "success":
                include "../src/view/messageRegister.php";
                break;
            default:
                include "../src/view/register.php";
                break;
        }
    }
    
    public function getMessage()
    {
        switch ($this->view)
        {
            case "loggedin":
                return "You are already logged in.";
            default:
                return "Registration successful.";
        }
    }
    
    public function hasErrors()
    {
        return $this->errors;
    }
    
    public function getError($key)
    {
        if (isset($this->errors[$key]))
        {
            return $this->errors[$key];
        }
        return "";
    }
    
    public function setUsername($input)
    {
        $this->username = $input;
        if (empty($this->username))
        {
            $this->errors["username"] = "A username is required.";
        }
    }
    
    public function getUsername()
    {
        return $this->username;
    }
    
    public function setEmail($input)
    {
        $this->email = $input;
        if (empty($this->email))
        {
            $this->errors["email"] = "A e-mail is required.";
        }
        else if (!filter_var($this->email, FILTER_VALIDATE_EMAIL))
        {
            $this->errors["email"] = "Please use a valid e-mail address.";
        }
    }
    
    public function getEmail()
    {
        return $this->email;
    }
    
    public function setPassword($input)
    {
        $this->password = $input;
        if (empty($this->password))
        {
            $this->errors["password"] = "A password is required.";
        }
    }
    
    public function setConfirmPassword($input)
    {
        $this->confirmPassword = $input;
        if (empty($this->confirmPassword))
        {
            $this->errors["confirmpassword"] = "Please confirm your password.";
        }
        else if (strcmp($this->password, $this->confirmPassword))
        {
            $this->errors["confirmpassword"] = "Password and confirmation did not match.";
        }
    }
}
$c_register = new RegisterController();