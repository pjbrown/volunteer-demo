<?php
require_once "../src/config/db.php";
/**
 * 
 */
class RegisterModel
{
    private $username;
    private $email;
    private $passwordHash;
    
    public function setUsername($input)
    {
        $this->username = $input;
    }
    
    public function setEmail($input)
    {
        $this->email = $input;
    }
    
    public function setPassword($input)
    {
        $this->passwordHash = password_hash($input, PASSWORD_DEFAULT);
    }
    
    public function validInsert()
    {
        $db = get_pdo();
        
        $stm = $db->prepare("SELECT username FROM volunteer_account WHERE username = :username");
        $stm->bindParam("username", $this->username);
        
        $stm->execute();
        
        return !($stm->rowCount() > 0);
    }
    
    public function insert()
    {
        $db = get_pdo();
        
        $stm = $db->prepare("INSERT INTO volunteer_account(username, password_hash, email) VALUES (:username, :password_hash, :email)");
        $stm->bindParam("username", $this->username, PDO::PARAM_STR);
        $stm->bindParam("password_hash", $this->passwordHash, PDO::PARAM_STR);
        $stm->bindParam("email", $this->email, PDO::PARAM_STR);
        
        $stm->execute();
    }
}
$m_register = new RegisterModel();