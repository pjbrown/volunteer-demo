<?php
require_once "../src/config/db.php";
class LoginModel
{
    private $username;
    private $password;
    
    public function setUsername($input)
    {
        $this->username = $input;
    }
    
    public function setPassword($input)
    {
        $this->password = $input;
    }
    
    public function authenticate()
    {
        $db = get_pdo();
        
        $stm = $db->prepare("SELECT password_hash FROM volunteer_account WHERE username = :username");
        $stm->bindParam("username", $this->username, PDO::PARAM_STR);
        
        $stm->execute();
        
        if ($stm->rowCount() == 1)
        {
            $hash = $stm->fetch(PDO::FETCH_ASSOC);
            return password_verify($this->password, $hash["password_hash"]);
        }
        else
        {
            return false;
        }
    }
}
$m = new LoginModel();