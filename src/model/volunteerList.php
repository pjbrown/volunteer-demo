<?php
require_once "../src/config/db.php";
/**
 * 
 * @author  Philipp J. Brown
 */
class VolunteerListModel
{
    private $list;
    
    public function __construct()
    {
        $db = get_pdo();
        
        $stm = $db->prepare("SELECT * FROM volunteer");
        $stm->execute();
        $this->list = $stm->fetchAll();
        
        foreach ($this->list as &$row)
        {
            $stm = $db->prepare("SELECT * FROM volunteer_availability WHERE volunteer_id = :volunteer_id");
            $stm->bindParam("volunteer_id", $row["volunteer_id"], PDO::PARAM_INT);
            $stm->execute();
            
            $row["availability"] = $stm->fetchAll();
        }
    }
    
    public function getList()
    {
        return $this->list;
    }
}
$m = new VolunteerListModel();