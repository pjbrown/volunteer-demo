<?php
require_once "../src/config/db.php";
/**
 * 
 * @author  Philipp J. Brown
 */
class VolunteerModel
{
    private $username;
    private $firstName;
    private $lastName;
    private $mobile;
    private $description;
    private $organisations;
    private $availability;
    private $mailing;
    
    public function setUsername($input)
    {
        $this->username = $input;
    }
    
    function getUsername()
    {
        return $this->username;
    }
    
    public function setFirstName($input)
    {
        $this->firstName = $input;
    }
    
    function getFirstName()
    {
        return $this->firstName;
    }
    
    public function setLastName($input)
    {
        $this->lastName = $input;
    }
    
    function getLastName()
    {
        return $this->lastName;
    }
    
    public function setMobile($input)
    {
        $this->mobile = $input;
    }
    
    function getMobile()
    {
        return $this->mobile;
    }
    
    public function setDescription($input)
    {
        $this->description = $input;
    }
    
    function getDescription()
    {
        return $this->description;
    }
    
    public function setOrganisations($input)
    {
        $this->organisations = $input;
    }
    
    function getOrganisations()
    {
        return $this->organisations;
    }
    
    public function setAvailability($input)
    {
        $this->availability = $input;
    }

    function getAvailability()
    {
        return $this->availability;
    }

    public function setMailing($input)
    {
        $this->mailing = $input;
    }
    
    public function getMailing()
    {
        return $this->mailing;
    }

    public function insert()
    {
        $db = get_pdo();
        
        $stm = $db->prepare("INSERT INTO volunteer(username, date_registration, firstname, lastname, mobile, presentation_description, organisations, mailinglist)"
                          . "VALUES (:username, :date_registration, :firstname, :lastname, :mobile, :presentation_description, :organisations, :mailinglist)");
        $stm->bindParam("username", $this->username, PDO::PARAM_STR);
        $stm->bindParam("date_registration", date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $stm->bindParam("firstname", $this->firstName, PDO::PARAM_STR);
        $stm->bindParam("lastname", $this->lastName, PDO::PARAM_STR);
        $stm->bindParam("mobile", $this->mobile, PDO::PARAM_STR);
        $stm->bindParam("presentation_description", $this->description, PDO::PARAM_STR);
        $stm->bindParam("organisations", $this->organisations, PDO::PARAM_STR);
        $stm->bindParam("mailinglist", $this->mailing, PDO::PARAM_INT);
        
        $stm->execute();
        
        // insert location records
        $id = $db->lastInsertId();
        foreach ($this->availability as $date => $location)
        {
            $stm_l = $db->prepare("INSERT INTO volunteer_availability(date_availability, volunteer_id, availability_location) VALUES (:date_availability, :volunteer_id, :availability_location)");
            $stm_l->bindParam("date_availability", $date, PDO::PARAM_STR);
            $stm_l->bindParam("volunteer_id", $id, PDO::PARAM_INT);
            $stm_l->bindParam("availability_location", $location, PDO::PARAM_STR);
            
            $stm_l->execute();
        }
    }
    
    public function getLast()
    {
        $db = get_pdo();
        
        $stm = $db->prepare("SELECT * FROM volunteer WHERE username = :username ORDER BY date_registration DESC LIMIT 1");
        $stm->bindParam("username", $this->username, PDO::PARAM_STR);
        
        $stm->execute();
        
        $arr = $stm->fetch();
        
        $this->setFirstName($arr["firstname"]);
        $this->setLastName($arr["lastname"]);
        $this->setMobile($arr["mobile"]);
        $this->setDescription($arr["presentation_description"]);
        $this->setOrganisations($arr["organisations"]);
        $this->setMailing($arr["mailinglist"]);
        
        return $stm->rowCount();
    }
}
$m = new VolunteerModel();