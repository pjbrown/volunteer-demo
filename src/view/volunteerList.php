<?php foreach($m->getList() as $record): ?>
<div class="table">
    <div class="table-left">
        <table>
            <tr>
                <th>Username</th>
                <th>Reg. Date</th>
                <th>Name</th>
                <th>Mobile</th>
                <th>M. List?</th>
            </tr>
            <tr>
                <td><?=$record["username"]?></td>
                <td><?=$record["date_registration"]?></td>
                <td><?=$record["firstname"] . " " . $record["lastname"]?></td>
                <td><?=$record["mobile"]?></td>
                <td><input type="checkbox" disabled <?=$record["mailinglist"] ? "checked" : ""?>></td>
            </tr>
        </table>
        <table class="description">
            <tr>
                <th>Presentation Desc.</th>
                <th>Organisations</th>
            </tr>
            <tr>
                <td><?=$record["presentation_description"]?></td>
                <td><?=$record["organisations"]?></td>
            </tr>
        </table>
    </div>
    <div class="table-right">
        <table>
            <tr>
                <th>Date</th>
                <th>Location</th>
            </tr>
<?php       foreach ($record["availability"] as $date): ?>
            <tr>
                <td><?=$date["date_availability"]?></td>
                <td><?=strtoupper($date["availability_location"])?></td>
            </tr>
<?php       endforeach; ?>
        </table>
    </div>
</div>
<?php endforeach; ?>