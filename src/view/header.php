<div class="bar">
    <ol class="bar-left">
        <li><a href="index.php">Instructions</a></li>
        <li><a href="register.php">Register</a></li>
        <li><a href="volunteer.php">Volunteer</a></li>
        <li><a href="volunteerList.php">Volunteer List</a></li>
    </ol>
<?php if (!$c_auth->isLoggedIn()): ?>
    <form class="bar-right" action="login.php" method="post">
        <label for="username">User Name: </label>
        <label for="password">Password: </label><br>
        <input type="text" id="username" name="username">
        <input type="password" id="password" name="password">
        <input type="submit" value="Log In">
    </form>
<?php else: ?>
    <span class="bar-right">[ Logged In: <?=$c_auth->getUsername()?> ] <a href="logout.php">Log Out</a></span>
<?php endif; ?>
</div>