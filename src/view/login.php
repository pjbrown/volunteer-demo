<form class="box" action="login.php" method="post">
    <h1>Login</h1>
    <span class="error"><?= $c->getError("login") ?></span>
    <p><span class="required">* Required</span></p>
    <p>
        <label class="big" for="username">User Name <span class="required">*</span></label>
        <input type="text" id="username" name="username" value="<?=$c->getUsername()?>">
        <span class="error"><?= $c->getError("username") ?></span>
    </p>
    <p>
        <label class="big" for="password">Password <span class="required">*</span></label>
        <input type="password" id="password" name="password">
        <span class="error"><?= $c->getError("password") ?></span>
    </p>
    <p>
        <input type="submit" value="Login">
    </p>
</form>