<form class="box" action="volunteer.php" method="post">
    <h1>Apply to present</h1>
<?php if (!$c_auth->isLoggedIn()): ?>
    <p class="center">Volunteering will also create an account. This account can be used to volunteer for future sessions without filling in this form again. <b>If you already have an account, please log in to automatically fill this form in with details from the last time you volunteered (if applicable).</b></p>
    <p><span class="required">* Required</span></p>
    <p>
        <label class="big" for="username">User Name <span class="required">*</span></label>
        <input type="text" name="username" id="username" value="<?= $c_register->getUsername() ?>">
        <span class="error"><?= $c_register->getError("username") ?></span>
    </p>
    <p>
        <label class="big" for="email">E-mail Address <span class="required">*</span></label>
        <input type="text" name="email" id="email" value="<?= $c_register->getEmail() ?>">
        <span class="error"><?= $c_register->getError("email") ?></span>
    </p>
    <p>
        <label class="big" for="password">Password <span class="required">*</span></label>
        <input type="password" name="password" id="password" value="">
        <span class="error"><?= $c_register->getError("password") ?></span>
    </p>
    <p>
        <label class="big" for="confirmpass">Confirm Password <span class="required">*</span></label>
        <input type="password" name="confirmpassword" id="confirmpass" value="">
        <span class="error"><?= $c_register->getError("confirmpassword") ?></span>
    </p>
    <hr>
<?php endif; ?>
<?php if ($c->isReturning()): ?>
    <p class="center"><b>We detected that you are a returning volunteer.</b> Your records from the last semesters can be updated to reflect the new semester.</p>
<?php endif; ?>
    <p class="center">Description...</p>
    <p>
        <label class="big" for="firstname">First Name <span class="required">*</span></label>
        <input type="text" name="firstname" id="firstname" value="<?= $c->getFirstName() ?>">
        <span class="error"><?= $c->getError("firstname") ?></span>
    </p>
    <p>
        <label class="big" for="lastname">Last Name <span class="required">*</span></label>
        <input type="text" name="lastname" id="lastname" value="<?= $c->getLastName() ?>">
        <span class="error"><?= $c->getError("lastname") ?></span>
    </p>
    <p>
        <label class="big" for="mobile">Mobile <span class="required">*</span></label>
        <input type="text" name="mobile" id="mobile" value="<?= $c->getMobile() ?>">
        <span class="error"><?= $c->getError("mobile") ?></span>
    </p>
    <hr>
    <p>
        <label class="big" for="description" maxlength="1000">Describe your presentation <span class="required">*</span></label>
        <textarea name="description" id="description" rows="4"><?= $c->getDescription() ?></textarea>
        <span class="error"><?= $c->getError("description") ?></span>
    </p>
    <p>
        <label class="big" for="organisations" maxlength="1000">Organisations <span class="required">*</span></label><br>
        Are you affiliated with any organisations? Student at ___, Employed by ___, etc? Please let us know.<br>
        <textarea name="organisations" id="organisations" rows="4"><?= $c->getOrganisations() ?></textarea>
        <span class="error"><?= $c->getError("organisations") ?></span>
    </p>
    <hr>
    <p>
        <label class="big">Your Availability (Curtin, ECU Joondalup, ECU Mt. Lawley, UWA)</label>
        <table>
            <tr>
                <th></th><th>Curtin University</th><th>ECU Joondalup</th><th>ECU Mt. Lawley</th><th>UWA</th><th>Any of the uni locations!</th>
            </tr>
<?php           foreach ($c->getDays() as $n => $date): ?>
                <tr>
                    <td><?= $c->getFormattedDate($date); ?></td>
                    <td><input type="radio" name="<?= $date ?>" value="curtin" <?= $c->getAvailability($date, "curtin") ? "checked" : ""?>></td>
                    <td><input type="radio" name="<?= $date ?>" value="joondalup" <?= $c->getAvailability($date, "joondalup") ? "checked" : ""?>></td>
                    <td><input type="radio" name="<?= $date ?>" value="lawley" <?= $c->getAvailability($date, "lawley") ? "checked" : ""?>></td>
                    <td><input type="radio" name="<?= $date ?>" value="uwa" <?= $c->getAvailability($date, "uwa") ? "checked" : ""?>></td>
                    <td><input type="radio" name="<?= $date ?>" value="all" <?= $c->getAvailability($date, "all") ? "checked" : ""?>></td>
                </tr>
<?php           endforeach; ?>
        </table>
        <span class="error"><?= $c->getError("availability") ?></span>
    </p>
    <p>
        <label class="big" class="big">Mailing List <span class="required">*</span></label><br>
        We include important information in our mailing list, including news relevant to volunteers.<br>
        Would you like to join the mailing list?<br>
        <input type="radio" name="mailing" id="mailingyes" value="1" <?= $c->isMailingChecked(true) ?>>
        <label for="mailingyes">Yes, sign me up! (Recommended)</label><br>
        <input type="radio" name="mailing" id="mailingno" value="0" <?= $c->isMailingChecked(false) ?>>
        <label for="mailingno">No thank you</label><br>
        <span class="error"><?= $c->getError("mailing") ?></span>
    </p>
    <p>
        <input type="submit" value="Submit">
    </p>
</form>