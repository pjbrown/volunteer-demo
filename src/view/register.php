<form class="box" action="register.php" method="post">
    <h1>Sign Up</h1>
    <p><span class="required">* Required</span></p>
    <p>
        <label class="big" for="username">User Name <span class="required">*</span></label>
        <input type="text" id="username" name="username" value="<?=$c_register->getUsername()?>">
        <span class="error"><?= $c_register->getError("username") ?></span>
    </p>
    <p>
        <label class="big" for="email">E-mail <span class="required">*</span></label>
        <input type="text" id="username" name="email" value="<?=$c_register->getEmail()?>">
        <span class="error"><?= $c_register->getError("email") ?></span>
    </p>
    <p>
        <label class="big" for="password">Password <span class="required">*</span></label>
        <input type="password" id="password" name="password">
        <span class="error"><?= $c_register->getError("password") ?></span>
    </p>
    <p>
        <label class="big" for="password">Confirm Password <span class="required">*</span></label>
        <input type="password" id="confirmpassword" name="confirmpassword">
        <span class="error"><?= $c_register->getError("confirmpassword") ?></span>
    </p>
    <p>
        <input type="submit" value="Sign Up">
    </p>
</form>