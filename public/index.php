<?php include "../src/controller/auth.php"; ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Volunteer Sign-up Demo</title>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Source+Sans+Pro:700,400">
    <link rel="stylesheet" href="style.css" type="text/css" media="screen">
</head>
<body>
<?php include "../src/view/header.php"; ?>

<div class="box">
    <h1>Volunteer Sign-up Demo</h1>
    <ol>
        <li>Try <a href="volunteer.php">Volunteering</a> without signing up.<br>Check the details you registered on the <a href="volunteerList.php">Volunteer List</a>.</li>
        <li>Try <a href="register.php">Signing Up</a> for an account.</li>
        <li>Now log in using the form in the menu.</li>
        <li>Try <a href="volunteer.php">Volunteering</a> while logged in.<br>Check the details you registered on the <a href="volunteerList.php">Volunteer List</a>.</li>
        <li>While still logged in, visit the <a href="volunteer.php">Volunteering</a> page.<br>The form should be automatically filled with data from the last sign-up.</li>
    </ol>
</div>
</body>
</html>