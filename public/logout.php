<?php
include "../src/controller/auth.php";
$c_auth->logout();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Volunteer Sign-up Demo</title>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Source+Sans+Pro:700,400">
    <link rel="stylesheet" href="style.css" type="text/css" media="screen">
</head>
<body>
<?php include "../src/view/header.php"; ?>

<div class="box">
    <p>You are now logged out.</p>
</div>
</body>
</html>