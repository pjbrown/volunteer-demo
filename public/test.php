<?php

$old_ays = [
    "av1" => "Saturday 22 August: 12:30pm - 2:30pm",
    "av2" => "Saturday 29 August: 12:30pm - 2:30pm",
    "av3" => "Saturday 5 September: 12:30pm - 2:30pm",
    "av4" => "Saturday 12 September: 12:30pm - 2:30pm",
    "av5" => "Saturday 19 September: 12:30pm - 2:30pm",
    "av6" => "Saturday 3 October: 12:30pm - 2:30pm",
    "av7" => "Saturday 10 October: 12:30pm - 2:30pm",
    "av8" => "Saturday 17 October: 12:30pm - 2:30pm"
];

$days = [
    "2015-08-22T12:30:00",
    "2015-08-29T12:30:00",
    "2015-09-05T12:30:00",
    "2015-09-12T12:30:00",
    "2015-09-19T12:30:00",
    "2015-10-03T12:30:00",
    "2015-10-10T12:30:00",
    "2015-10-17T12:30:00",
];

?>

<pre>
<?php
    foreach ($days as $day)
    {
        $date = new DateTimeImmutable($day);
        $end = $date->add(new DateInterval("PT2H"));
        echo $date->format("l j F: g:ia") . " - " . $end->format("g:ia");
        echo "\n";
    }
?>
</pre>