<?php
include "../src/controller/auth.php";
include "../src/model/register.php";
include "../src/controller/register.php";
include "../src/model/volunteer.php";
include "../src/controller/volunteer.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Volunteer Sign-up Demo</title>
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Source+Sans+Pro:700,400">
    <link rel="stylesheet" href="style.css" type="text/css" media="screen">
</head>
<body>
<?php include "../src/view/header.php"; ?>

<?php $c->showView(); ?> 
</body>
</html>